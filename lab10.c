#include <stdio.h>

int main()
{    
    FILE *fp;
    char ch;
    fp=fopen("input.txt","w");
	printf("enter data:\n");
    while((ch=getchar())!=EOF)
    {
        putc(ch,fp);
    }
    fclose(fp);
    fp=fopen("input.txt","r");
    printf("files contents are:\n");
    while((ch=getc(fp))!=EOF)
    {
        printf("%c",ch);
    }
    fclose(fp);
    return 0;
    
}