
#include <stdio.h>
int main() 
{
    int a[3][2]={{3,5},{5,6},{7,8}}, b[2][3],i, j;
    
    printf("Given matrix: \n");
    for (i = 0; i <3 ;i++)
        for (j = 0; j < 2;j++) 
            {
            printf("%d\t", a[i][j]);
            if (j == 1)
                printf("\n");
        }

    for (i = 0; i < 3;i++)
        for (j = 0; j < 2;j++)
        {
            b[j][i] = a[i][j];
        }

    printf("\nTranspose of the matrix:\n");
   for (i = 0; i < 2;i++)
        for (j = 0; j < 3;j++)
 {
            printf("%d ", b[i][j]);
            if (j == 2)
                printf("\n");
        }
    return 0;
}